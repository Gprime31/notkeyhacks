# How to contribute

If you can, the best way to contribute is to [fork](https://gitlab.com/dee-see/notkeyhacks/-/forks/new) the project, make the desired change and [open a merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream).

The format is:

```markdown
#### Short description

- First reference showing it's not sensitive
- Second reference showing it's not sensitive
- ...
```

Alternatively you can post your token and references in an [issue](https://gitlab.com/dee-see/notkeyhacks/-/issues/new) and I will add the information to the repository.
